var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  walker: [0, 0, 0],
  path: [128, 128, 128],
  back: [64, 64, 64]
}

var size = 11
var boardSize
var mazeMap = []
for (var i = 0; i < size; i++) {
  mazeMap.push(create2DArray(size, size, 1, true))
}
var walker = {
  x: null,
  y: null,
  z: null
}
var neighbours = []
walker.x = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
walker.y = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
walker.z = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
mazeMap[walker.x][walker.y][walker.z] = 0.5
var backTrack = []
var frames = 0
var inProg = true

var checkMap = []
for (var i = 0; i < size; i++) {
  checkMap.push(create2DArray(size, size, 0, true))
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function draw() {
  createCanvas(windowWidth, windowHeight, WEBGL)
  background(0)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  lights()

  translate(0, 0, -size * (42 / 768) * boardSize * (17 / size))
  push()
  rotateY(frameCount * 0.01)
  rotateZ(frameCount * 0.015)
  rotateX(frameCount * 0.05)
  for (var i = 0; i < mazeMap.length; i++) {
    for (var j = 0; j < mazeMap[i].length; j++) {
      for (var k = 0; k < mazeMap[i][j].length; k++) {
        if (mazeMap[i][j][k] !== 1) {
          push()
          translate((i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (k - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size))
          stroke(255)
          strokeWeight(4)
          fill(2 * 255 * mazeMap[i][j][k])
          box((42 / 768) * boardSize * (17 / size) * 1.00, (42 / 768) * boardSize * (17 / size) * 1.00, (42 / 768) * boardSize * (17 / size) * 1.00)
          pop()
        }
      }
    }
  }
  pop()

  frames += deltaTime * 0.025
  if (frames > 1 && inProg === true) {
    frames = 0

    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        for (var k = 0; k < mazeMap[i][j].length; k++) {
          if (mazeMap[i][j][k] !== 1) {
            checkMap[i][j][k] = mazeMap[i][j][k]
          }
        }
      }
    }

    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        for (var k = 0; k < mazeMap[i][j].length; k++) {
          if (mazeMap[i][j][k] !== 1 && mazeMap[i][j][k] > 0) {
            mazeMap[i][j][k] -= 0.0025 * (57 / size)
          } else if (mazeMap[i][j][k] !== 1 && mazeMap[i][j][k] <= 0) {
            mazeMap[i][j][k] = 0
          }
        }
      }
    }

    neighbours = getNeighbours(walker.x, walker.y, walker.z, mazeMap)
    var rand = Math.floor(Math.random() * neighbours.length)
    if (neighbours[rand] !== undefined) {
      backTrack.push([walker.x, walker.y, walker.z])
      walker.x = neighbours[rand][0]
      walker.y = neighbours[rand][1]
      walker.z = neighbours[rand][2]
      mazeMap[walker.x][walker.y][walker.z] = 0.5
      if (neighbours[rand][3] === 0) {
        mazeMap[walker.x - 1][walker.y][walker.z] = 0.5
        backTrack.push([walker.x - 1, walker.y, walker.z])
      } else if (neighbours[rand][3] === 1) {
        mazeMap[walker.x + 1][walker.y][walker.z] = 0.5
        backTrack.push([walker.x + 1, walker.y, walker.z])
      } else if (neighbours[rand][3] === 2) {
        mazeMap[walker.x][walker.y - 1][walker.z] = 0.5
        backTrack.push([walker.x, walker.y - 1, walker.z])
      } else if (neighbours[rand][3] === 3) {
        mazeMap[walker.x][walker.y + 1][walker.z] = 0.5
        backTrack.push([walker.x, walker.y + 1, walker.z])
      } else if (neighbours[rand][3] === 4) {
        mazeMap[walker.x][walker.y][walker.z - 1] = 0.5
        backTrack.push([walker.x, walker.y, walker.z - 1])
      } else if (neighbours[rand][3] === 5) {
        mazeMap[walker.x][walker.y][walker.z + 1] = 0.5
        backTrack.push([walker.x, walker.y, walker.z + 1])
      }
    } else {
      if (backTrack.length !== 0) {
        walker.x = backTrack[backTrack.length - 2][0]
        walker.y = backTrack[backTrack.length - 2][1]
        walker.z = backTrack[backTrack.length - 2][2]
        mazeMap[backTrack[backTrack.length - 1][0]][backTrack[backTrack.length - 1][1]][backTrack[backTrack.length - 1][2]] = 0.5
        mazeMap[walker.x][walker.y][walker.z] = 0.5
        backTrack.splice(-2, 2)
      } else {
        if (Math.max(...checkMap.flat().flat()) === 0) {
          setTimeout(function() {
            mazeMap = []
            for (var i = 0; i < size; i++) {
              mazeMap.push(create2DArray(size, size, 1, true))
            }
            walker.x = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
            walker.y = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
            walker.z = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
            backTrack = []
            neighbours = []
            inProg = false
            setTimeout(function() {
              inProg = true
            }, 0)
            checkMap = []
            for (var i = 0; i < size; i++) {
              checkMap.push(create2DArray(size, size, 0, true))
            }
          }, 2500)
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(x, y, z, array) {
  var neighbour = []
  if (x < array.length - 2) {
    if (array[x + 2][y][z] === 1) {
      neighbour.push([x + 2, y, z, 0])
    }
  }
  if (x > 2) {
    if (array[x - 2][y][z] === 1) {
      neighbour.push([x - 2, y, z, 1])
    }
  }
  if (y < array.length - 2) {
    if (array[x][y + 2][z] === 1) {
      neighbour.push([x, y + 2, z, 2])
    }
  }
  if (y > 2) {
    if (array[x][y - 2][z] === 1) {
      neighbour.push([x, y - 2, z, 3])
    }
  }
  if (z < array.length - 2) {
    if (array[x][y][z + 2] === 1) {
      neighbour.push([x, y, z + 2, 4])
    }
  }
  if (z > 2) {
    if (array[x][y][z - 2] === 1) {
      neighbour.push([x, y, z - 2, 5])
    }
  }
  return neighbour
}
